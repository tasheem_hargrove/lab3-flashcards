package cs.mad.flashcards.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet

class FlashcardSetDetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flashcard_set_detail)

        val recyclerView = findViewById<RecyclerView>(R.id.recView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        val adapter = FlashcardAdapter()
        recyclerView.adapter = adapter

        val addBtn = findViewById<Button>(R.id.addBtn)
        addBtn.setOnClickListener {
            adapter.cards.add(Flashcard("New Card", "New definition"))
            adapter.notifyDataSetChanged()
        }

        val deleteBtn = findViewById<Button>(R.id.deleteBtn)
        deleteBtn.setOnClickListener {
            adapter.cards.removeLast()
            adapter.notifyDataSetChanged()
        }
    }
}